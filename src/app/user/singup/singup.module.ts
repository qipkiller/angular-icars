import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SingupRoutingModule } from './singup-routing.module';
import { SingupComponent } from './singup/singup.component';
import { SingupjuridicComponent } from './singupjuridic/singupjuridic.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    SingupRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [SingupComponent, SingupjuridicComponent]
})
export class SingupModule { }
