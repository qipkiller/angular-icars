import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { checkIfMatchingPasswords } from 'src/app/validators/confirmPassword.validator';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { UserService } from 'src/app/shared/user/user.service';
import { Router } from '@angular/router';
declare var $: any;

@Component({
  selector: 'app-singupjuridic',
  templateUrl: './singupjuridic.component.html',
  styleUrls: ['./singupjuridic.component.css']
})
export class SingupjuridicComponent implements OnInit {
  submitted_juridic = false;
  errors = true;
  singUpJuridicForm: FormGroup;
  httpValidation = [];

  submitGeneral = [
    'userType', 'name', 'login',
    'email', 'number', 'password', 'link',
    'adminName', 'adminPhone',
    'city', 'address', 'municipality', 'description',
  ];

  service = Array(
    'electricians', 'engine', 'conditioner', 'brake',
    'glass', 'exhaustSystem', 'transmission', 'diagnostics',
    'runningGear', 'body', 'polishing',
    'dents', 'spares', 'cars',
  );
  showroom = Array(
    'redemption', 'tradein', 'leasing', 'credit',
    'partialRedemption', 'onlyNewAuto',
  );

  tuning = Array(
    'toning', 'soundproofing', 'vinyl', 'engineTuning',
    'transmissionTuning', 'bodyTuning', 'undercarriageTuning',
    'chipTuning', 'cars',
  );
  fitting = Array(
    'correctionIron', 'correctionCast', 'vulcanization',
    'discShop', 'tireShop',
  );
  wash = Array(
    'washing', 'dryCleaning', 'washPolishing', 'nanoceramics',
    'membrane', 'washVinyl', 'cleaningDiscs', 'washSoundproofing',
  );
  submitAll = {
    'service': this.service, 'showroom': this.showroom,
    'tuning': this.tuning, 'fitting': this.fitting,
    'wash': this.wash
  };
  category = {service: false, showroom: false, tuning: false, fitting: false, wash: false};

  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
    private router: Router,
    ) {}

  ngOnInit() {
    $(document).ready(function() {
      $('select').formSelect();
    });
    this.singUpJuridicForm = this.formBuilder.group({
      userType: ['', [Validators.required]],
      name: ['name', [Validators.required, Validators.pattern(/[\p{L}\s]+/), Validators.minLength(2), Validators.maxLength(16)]],
      description: [''],
      login: ['login', [Validators.required, Validators.pattern(/[A-Za-z0-9\s.]+/), Validators.minLength(4), Validators.maxLength(16)]],
      email: ['qipkiller@gmail.com', [Validators.required, Validators.email]],
      phone: ['+37360610835',
        [Validators.required, Validators.pattern(/^(\+)?((\s)?((\()?(-)?([0-9]+)?(\))?))+/), Validators.maxLength(20)]],
      password: ['parola111', [Validators.required, Validators.minLength(6)]],
      confirmPass: ['parola111', [Validators.required]],
      municipality: ['', ],
      city: ['', ],
      address: ['', ],
      link: [''],
      adminName: ['', ],
      adminPhone: [''],
      // service
      electricians: [''],
      conditioner: [''],
      engine: [''],
      brake: [''],
      glass: [''],
      exhaustSystem: [''],
      transmission: [''],
      diagnostics: [''],
      runningGear: [''],
      body: [''],
      polishing: [''],
      dents: [''],
      spares: [''],
      cars: [''],
      // showroom
      redemption: [''],
      tradein: [''],
      leasing: [''],
      credit: [''],
      partialRedemption: [''],
      onlyNewAuto: [''],
      // tuning
      toning: [''],
      soundproofing: [''],
      vinyl: [''],
      engineTuning: [''],
      transmissionTuning: [''],
      bodyTuning: [''],
      undercarriageTuning: [''],
      chipTuning: [''],
      // fitting
      correctionIron: [''],
      correctionCast: [''],
      vulcanization: [''],
      discShop: [''],
      tireShop: [''],
      // wash
      washing: [''],
      dryCleaning: [''],
      washPolishing: [''],
      nanoceramics: [''],
      membrane: [''],
      washVinyl: [''],
      cleaningDiscs: [''],
      washSoundproofing: [''],

    }, {validator: checkIfMatchingPasswords('password', 'confirmPass')});

  }

  get f() { return this.singUpJuridicForm.controls; }
  get value() { return JSON.stringify(this.singUpJuridicForm.value); }


  onChange(deviceValue) {
    $(document).ready(function() {
      $('select').formSelect();
    });
    // obnulenie
    this.category = {service: false, showroom: false, tuning: false, fitting: false, wash: false};

    if (deviceValue === 'service') {
      this.category.service = true;
    }
    if (deviceValue === 'showroom') {
      this.category.showroom = true;
    }
    if (deviceValue === 'tuning') {
      this.category.tuning = true;
    }
    if (deviceValue === 'fitting') {
      this.category.fitting = true;
    }
    if (deviceValue === 'wash') {
      this.category.wash = true;
    }
  }
  onSubmitJuridic() {
    this.submitted_juridic = true;
    // stop here if form is invalid
/*     if (this.singUpJuridicForm.invalid) {
      return;
    } */
    let temp = [];
    // alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.singUpJuridicForm.value))
    for (const el in this.category) {
      if (this.category[el]) {
        temp = this.submitAll[el].concat(this.submitGeneral);
      }
    }
    const Submit = {};
    // tslint:disable-next-line:forin
    for (const el in temp) {
      // Use `key` and `value`
     // console.log(Submit[el] + ' : ' + this.singUpJuridicForm.value[Submit[el]]);

      // submit this ---> this.singUpJuridicForm.value[Submit[el]]
    
      Submit[temp[el]] = this.singUpJuridicForm.value[temp[el]];
    }

    this.userService.registrationJur(Submit).subscribe((res: HttpResponse<any>) => {
      console.log(res);
      if (res.ok) {
        this.router.navigate(['']);
      }
    },
    (err: HttpErrorResponse) => {
      if (err.error.VALIDATION) {
        this.httpValidation = err.error.VALIDATION;
      }
    });
    this.submitted_juridic = false;
  }

}
