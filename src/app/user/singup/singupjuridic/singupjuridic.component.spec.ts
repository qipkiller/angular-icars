import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SingupjuridicComponent } from './singupjuridic.component';

describe('SingupjuridicComponent', () => {
  let component: SingupjuridicComponent;
  let fixture: ComponentFixture<SingupjuridicComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SingupjuridicComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingupjuridicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
