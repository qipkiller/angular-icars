import { Component, OnInit, AfterViewInit} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../../../shared/user/user.service';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { checkIfMatchingPasswords } from 'src/app/validators/confirmPassword.validator';
import { AuthService } from 'src/app/auth/auth.service';

declare var $: any;


@Component({
  selector: 'app-singup',
  templateUrl: './singup.component.html',
  styleUrls: ['./singup.component.css'],
})
export class SingupComponent implements OnInit, AfterViewInit {
  ngAfterViewInit() {
    $(document).ready(function() {
      $('.tabs').tabs();
    });
  }
  // tslint:disable-next-line:member-ordering
  singUpFizicForm: FormGroup;
  // tslint:disable-next-line:member-ordering
  submitted_fizic = false;
  // tslint:disable-next-line:member-ordering
  httpValidation = [];

  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
    private router: Router,
    private authService: AuthService) { }

  ngOnInit() {
    this.singUpFizicForm = this.formBuilder.group({
      name: ['Sarev Alexandru', [Validators.required, Validators.pattern(/[\p{L}\s]+/), Validators.minLength(2), Validators.maxLength(16)]],
      login: ['login', [Validators.required, Validators.pattern(/[A-Za-z0-9\s.]+/), Validators.minLength(4), Validators.maxLength(16)]],
      email: ['qipkiller@gmail.com', [Validators.required, Validators.email]],
      phone: ['+37360610835',
        [Validators.required, Validators.pattern(/^(\+)?((\s)?((\()?(-)?([0-9]+)?(\))?))+/), Validators.maxLength(20)]],
      password: ['1234Aaaa', [Validators.required, Validators.pattern(/[A-Za-z0-9]+/), Validators.minLength(4), Validators.maxLength(16)]],
      confirmPass: ['1234Aaaa', [Validators.required]]
    }, {validator: checkIfMatchingPasswords('password', 'confirmPass')});
  }


  // convenience getter for easy access to form fields
  get f() { return this.singUpFizicForm.controls; }
  get value() { return JSON.stringify(this.singUpFizicForm.value); }
  onSubmitFizic() {
    this.httpValidation = [];
    this.submitted_fizic = true;

    // stop here if form is invalid
    if (this.singUpFizicForm.invalid) {
        return;
    }
    this.userService.registrationPhyz(this.singUpFizicForm.value).subscribe((res: HttpResponse<any>) => {
      console.log(res);
      if (res.ok) {
        this.router.navigate(['']);
      }
    },
    (err: HttpErrorResponse) => {
      if (err.error.VALIDATION) {
        this.httpValidation = err.error.VALIDATION;
      }
    });
    this.submitted_fizic = false;
  }

}
