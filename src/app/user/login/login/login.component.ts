import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Modal } from 'materialize-css';
import { UserService } from '../../../shared/user/user.service';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  submitted: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
    private router: Router,
    private authService: AuthService
    ) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
        login: ['1234', [Validators.required]],
        password: ['1234', [Validators.required]],
    });
  }
  // convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }

  toSingUp() {
    const elem = document.getElementById('login_modal');
    const instance = Modal.getInstance(elem);
    instance.close();
  }
  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
        return;
    }
    const userName = this.loginForm.controls.login.value;
    const password = this.loginForm.controls.password.value;

    this.userService.userAuthentication(userName, password).subscribe((res: HttpResponse<any>) => {
      this.authService.pushUser(res.body);
      const elem = document.getElementById('login_modal');
      const instance = Modal.getInstance(elem);
      instance.close();
      if (this.router.routerState.snapshot.url === '/singup') {
        this.router.navigate(['']);
      }
    },
    (err: HttpErrorResponse) => {
      this.submitted = false;
      console.log(err.message);
    });
  }
}
