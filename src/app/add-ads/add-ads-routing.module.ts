import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddAdsComponent } from './add-ads/add-ads.component';

const routes: Routes = [
  { path: '', component: AddAdsComponent }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [RouterModule],
})
export class AddAdsRoutingModule { }
