import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl, FormArray } from '@angular/forms';
import { AddAdsService } from './add-ads.service';
import { AdsService } from 'src/app/shared/ads/ads.service';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';


@Component({
  selector: 'app-add-ads',
  templateUrl: './add-ads.component.html',
  styleUrls: ['./add-ads.component.css']
})
export class AddAdsComponent implements OnInit {
  constructor(
    private formBuilder: FormBuilder,
    private addAdsService: AddAdsService,
    private adsService: AdsService
    ) {}
  aaTransportTypes = this.addAdsService.transportTypes;
  addForm: FormGroup;
  separated: FormArray;
  resulted: boolean;
  resultData = [];
  separatedNames = [];
  years = [];
  currentBrands = [];
  currentModels = [];
  submitted = false;

  initForm() {
    this.addForm = this.formBuilder.group({
      transportType: '',
      brand: '',
      model: '',
      price: '',
      power: '',
      year: '',
      mileage: '',
      separated: this.formBuilder.array([])
    });
  }

  ffunc(key, value, result) {
    if (typeof(value) === 'object') {
      this.func(value, result);
    } else if (typeof(value) === 'string') {
      result[key] = value;
    }
    return result;
  }

  func(object, result) {
    Object.entries(object).forEach(
      ([key, value]) => this.ffunc(key, value, result)
    );
    return result;
  }

  SeparatedName(index) {
    return this.separatedNames[index];
  }


  getAddAdsForm() {
    // init transport type
    this.adsService.getAllTransportType().subscribe((res: HttpResponse<any>) => {
      this.addAdsService.transportTypes = res.body.transportTypes;
      this.addAdsService.models = res.body.models;
      this.addAdsService.brands = res.body.brands;
      this.addAdsService.engines = res.body.engines;
      this.addAdsService.bodies = res.body.bodies;
      this.SelectInit();
    },
    ( err: HttpErrorResponse ) => {
      console.log(err.message);
    });
  }



  SelectInit() {
    $(document).ready(function() {
      $('select').formSelect();
    });
/*     $(document).ready(function() {
      $('input#power, input#price, input#mileage, input#additionalInfo').characterCounter();
    }); */
  }
  SelectReset(select) {
    select.forEach(selectt => {
      this.addForm.controls[selectt].reset('');
    });
  }
  ngOnInit() {
    this.getAddAdsForm();
    for (let y = new Date().getFullYear(); y >= 1900; y--) {
      this.years.push({value: y, name: y});
    }
    this.initForm();
    this.SelectInit();
  }
  get valueee() {return JSON.stringify(this.addForm.value); }
  get separatedForm() { return <FormArray>this.addForm.get('separated'); }
  get f() { return this.addForm.controls; }

  onChangeType(_type) {
    this.SelectReset(['model', 'brand']);
    let group = {};
    this.separated = this.addForm.get('separated') as FormArray;

    const lenght = this.separated.length;
    for ( let i = 0; i <= lenght; i++) {
      this.separated.removeAt(0);
    }
    this.addAdsService.transportTypes.forEach( transportType => {
      const off = ['year', 'brands', 'id', 'name', 'price', 'power', 'mileage'];
      const dropdowns = [
        'municipality', 'registration',
        'transmission', 'body', 'engine',
        'eurostandart', 'state', 'color',
        'coolingType', 'term'
      ];
      const _switch  = [
        'bargain'
      ];
      // tslint:disable-next-line:'radix'
      if ( transportType.name === _type ) {
        this.currentBrands = transportType.brands;
        for (const index in transportType) {
          if ( transportType.hasOwnProperty(index)) {
            const attr = transportType[index];
            if (attr === true) {
              if (off.includes(index) !== true) {
                let inputType = 'checkbox';
                let value = 'false';
                if (dropdowns.includes(index) === true) {
                  inputType = 'dropdown';
                  if (!transportType[index].values) {
                    value = '';
                  } else {
                    value = transportType[index].values;
                  }
                } else if (_switch.includes(index) === true) {
                  inputType = 'switch';
                }
                group[index] = value;
                this.separated.push(
                  this.formBuilder.group(group)
                );
                const sep = {
                  name: index,
                  type: inputType
                };
                this.separatedNames.push(sep);
                group = {};
              }
            }
          }
        }
      }
    });
    this.SelectInit();
  }

  onChangeBrand(brand) {
    this.currentBrands.forEach( Brand => {
      // tslint:disable-next-line:radix
      if ( Brand.name === brand ) {
        this.currentModels = Brand.models;
      }
    });
    this.SelectInit();
  }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.addForm.invalid) {
      return;
    }
    const copy = this.func(this.addForm.value, {});
    this.adsService.sendFormDataAddAds(copy).subscribe((res: HttpResponse<any>) => {
      if (res.body.length > 0 ) {
        console.log(res.body);
        this.resulted = true;
        this.resultData = res.body;
      } else {
        this.resulted = false;
        this.resultData = [];
      }
    },
    ( err: HttpErrorResponse ) => {
      console.log(err.message);
    });
  }

}
