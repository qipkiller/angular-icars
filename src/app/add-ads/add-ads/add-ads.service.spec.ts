import { TestBed, inject } from '@angular/core/testing';

import { AddAdsService } from './add-ads.service';

describe('AddAdsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AddAdsService]
    });
  });

  it('should be created', inject([AddAdsService], (service: AddAdsService) => {
    expect(service).toBeTruthy();
  }));
});
