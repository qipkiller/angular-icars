import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddAdsRoutingModule } from './add-ads-routing.module';
import { AddAdsComponent } from './add-ads/add-ads.component';
import { ReactiveFormsModule } from '@angular/forms';
import { TranslateModule, TranslateLoader, TranslateService } from '@ngx-translate/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { createTranslateLoader } from '../app.module';


@NgModule({
  imports: [
    CommonModule,
    AddAdsRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient],
      }
    })
  ],
  declarations: [AddAdsComponent]
})
export class AddAdsModule {
}








