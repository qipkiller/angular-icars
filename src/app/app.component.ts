import { Component, OnInit} from '@angular/core';
import { Modal } from 'materialize-css';
import { Sidenav } from 'materialize-css';
import { Dropdown , FormSelect } from 'materialize-css';
import { LanguageService } from './language.service';
import { UserService } from './shared/user/user.service';
import { Router } from '@angular/router';
import { AuthService } from './auth/auth.service';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import * as jwt from 'jsonwebtoken';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {

  title = 'PROJECT-NAME';
  currentLang: string;

  constructor (
    private userService: UserService,
    private languageService: LanguageService,
    private router: Router,
    private authService: AuthService
    ) {
      this.currentLang = languageService.currentLang;
/*     // get uri authentificated
    this.userService.whoIsAuth().subscribe((res: HttpResponse<any>) => {
      if (res.body === 'NoSuchUserMessage') {
        this.router.navigate(['']);
      } else {
        this.authService.pushUser(res.body);
      }
    },
    (err: HttpErrorResponse) => {
      console.log(err.message);
    }); */
  }
  initMaterialize() {
    document.addEventListener('DOMContentLoaded', function() {
      const elem = document.querySelectorAll('.modal');
      const options = {
        'dismissible' : true,
        'preventScrolling' : false,
        'opacity' : 0.4
      };
      Modal.init(elem, options);
    });
    document.addEventListener('DOMContentLoaded', function() {
      const elems = document.querySelectorAll('.sidenav');
      const options = {
        inDuration: 200
      };
      Sidenav.init(elems, options);
    });
    document.addEventListener('DOMContentLoaded', function() {
      const elems = document.querySelectorAll('.dropdown-trigger');
      const options = {
        coverTrigger: false
      };
      Dropdown.init(elems, options);
    });
    document.addEventListener('DOMContentLoaded', function() {
      const elems = document.querySelectorAll('#login.dropdown-trigger');
      const options = {
        constrainWidth: false,
        coverTrigger: false
      };
      Dropdown.init(elems, options);
    });
  }

  get User() { return this.authService.getUser(); }

  isLogin() {
    if (this.authService.isLogin()) {
      this.initMaterialize();
      return true;
    }
    return false;
  }

  Logout() {
    this.userService.logout().subscribe((res: HttpResponse<any>) => {
      if (res.body === 'Logged out') {
        this.authService.removeUser();
        this.router.navigate(['']);
      } else {
        // return error
      }
    },
      (err: HttpErrorResponse) => {

        console.log(err.message);
      });
  }

  SelectInit() {
    $(document).ready(function() {
      $('select').formSelect();
    });
  }

  switchLanguage(language: string) {
    this.languageService.use(language).subscribe(
      {
        complete: () => $('select').formSelect()
      }
    );
  }

  ngOnInit() {
    this.SelectInit();
    this.initMaterialize();
  }
}
