import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injectable } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {
  HttpClientModule,
  HttpClient, HttpInterceptor,
  HttpRequest, HttpHandler, HTTP_INTERCEPTORS, HttpEvent } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { AppComponent } from './app.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ReactiveFormsModule} from '@angular/forms';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { IndexComponent } from './index/index.component';
import { SearchComponent } from './search/search.component';
import { LoginComponent } from './user/login/login/login.component';
import { Observable } from 'rxjs';
import { JwtModuleOptions, JwtModule } from '@auth0/angular-jwt';
import { NonAuthGuard } from './auth/non-auth.guard';
import { UserComponent } from './user/user.component';

export function jwtTokenGetter() {
  return sessionStorage.getItem('userToken');
}

const routes: Routes = [
  { path: '', component: IndexComponent},
  { path: 'singup', loadChildren: './user/singup/singup.module#SingupModule', canActivate: [NonAuthGuard] },
  { path: 'add', loadChildren: './add-ads/add-ads.module#AddAdsModule'},
  { path: 'profile', loadChildren: './profile/profile.module#ProfileModule'},
  { path: 'ad/:id', loadChildren: './ads-page/ads-page.module#AdsPageModule'},
  {path: '404', component: PageNotFoundComponent},
  {path: '**', redirectTo: '/404'}
];
@Injectable()
export class MyInterceptorService implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // log the request url
    const req_cloned = req.clone({
      withCredentials: true,
    });
    return next.handle(req_cloned);
  }
}
const JWT_Module_Options: JwtModuleOptions = {
  config: {
      tokenGetter: jwtTokenGetter,
      whitelistedDomains: ['localhost:4200']
  }
};
@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent,
    IndexComponent,
    SearchComponent,
    LoginComponent,
    UserComponent,
  ],
  imports: [
    BrowserModule,
    FontAwesomeModule,
    ReactiveFormsModule,
    JwtModule.forRoot(JWT_Module_Options),
    RouterModule.forRoot(
      routes,
       // { enableTracing: true } // <-- debugging purposes only
    ),
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      },
    })
  ],
  exports: [
    RouterModule,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: MyInterceptorService,
      multi: true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {

}

export function createTranslateLoader(http: HttpClient) {

  return new TranslateHttpLoader(http, '../assets/i18n/root/', '.json');
  // new TranslateHttpLoader(http, 'http://185.225.17.40/', '.php');
}
