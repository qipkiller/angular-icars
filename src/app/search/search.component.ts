import { Component, OnInit, OnChanges, SimpleChanges, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray} from '@angular/forms';
import { AdsService } from '../shared/ads/ads.service';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { LanguageService } from '../language.service';
import { SearchService } from './search.service';
import { Collapsible, FormSelect } from 'materialize-css';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { Ads, Inputs } from '../shared/models/ads.model';
import { Router } from '@angular/router';


@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css'],
})
// tslint:disable-next-line:label-position

export class SearchComponent implements OnInit {
  constructor(private formBuilder: FormBuilder,
    private languageService: LanguageService,
    private adsService: AdsService,
    private searchService: SearchService,
    private http: HttpClient,
    private router: Router
    ) {
  }
  searchForm: FormGroup;
  averageForm: FormGroup;
  separated: FormArray;
  separatedNames = [];
  submitted = false;
  resulted = false;
  more = false;
  resultData: Ads;
  currentBrands = [];
  currentBrands2 = [];
  currentModels = [];
  currentModels2 = [];
  yearsFrom = [];
  yearsTo = [];
  Average = 0;
  averageSubmitted = false;
  initForm() {
    this.searchForm = this.formBuilder.group({
      transportType: '',
      brand: '',
      model: '',
      priceFrom: '',
      priceTo: '',
      powerFrom: '',
      powerTo: '',
      yearFrom: '',
      yearTo: '',
      mileageFrom: '',
      mileageTo: '',
      separated: this.formBuilder.array([])
    });

    this.averageForm = this.formBuilder.group({
      transportType: '',
      brand: '',
      model: '',
      year: '',
      engine: '',
    });

  }

  ffunc (key, value, result) {
    if (typeof(value) === 'object') {
      this.func(value, result);
    } else if (typeof(value) === 'string') {
      result[key] = value;
    }
    return result;
  }

  func(object, result) {
    Object.entries(object).forEach(
      ([key, value]) => this.ffunc(key, value, result)
    );
    return result;
  }

  SeparatedName(index) {
    return this.separatedNames[index];
  }
  ColapsInit() {
    console.log('init colapse');
    const colaps = document.querySelectorAll('.collapsible');
    Collapsible.init(colaps, {
      'onCloseEnd': function(el) {
        console.log('net colaps');
      }
    });
  }
  SelectInit() {
    const select = document.querySelectorAll('select');
    FormSelect.init(select);
  }

  SelectReset(select: string[]) {
    select.forEach((selectt: string) => {
      this.searchForm.controls[selectt].reset('');
    });
  }
  getSearchForm() {
    // init transport type
    this.adsService.getAllTransportType().subscribe((res: HttpResponse<any>) => {
      this.searchService.transportTypes = res.body.transportTypes;
      this.searchService.models = res.body.models;
      this.searchService.brands = res.body.brands;
      this.searchService.engines = res.body.engines;
      this.searchService.bodies = res.body.bodies;
      this.SelectInit();
    },
    ( err: HttpErrorResponse ) => {
      console.log(err.message);
    });
  }

  onChangeYear(yearsFrom: number) {
    this.yearsTo = [];
    for (let y = new Date().getFullYear(); y >= yearsFrom; y--) {
      this.yearsTo.push({value: y, name: y});
    }
    this.SelectInit();
  }

  changeMore(more: boolean) {
    this.more = more;

    this.onChangeType(this.searchForm.get('transportType').value);
  }
  ngOnInit() {
    this.getSearchForm();
    console.log('ngOnInit');
    for (let y = new Date().getFullYear(); y >= 1900; y--) {
      this.yearsFrom.push({value: y, name: y});
    }
    this.initForm();
    this.SelectInit();
    this.ColapsInit();
  }

  get f() { return this.searchForm.controls; }
  get ssTransportType() { return this.searchService.transportTypes; }
  onChangeType(_type: string) {
    this.SelectReset(['model', 'brand']);
    let group = {};
    this.separated = this.searchForm.get('separated') as FormArray;

    const lenght = this.separated.length;
    for ( let i = 0; i <= lenght; i++) {
      this.separated.removeAt(0);
    }
    this.searchService.transportTypes.forEach( transportType => {

      if ( transportType.name === _type ) {
        this.currentBrands = transportType.brands;
        if (this.more) {
          const off = [
            'year', 'brands', 'id', 'name',
            'price', 'power', 'mileage'
          ];
          const dropdowns = [
            'municipality', 'registration',
            'transmission', 'body', 'engine',
            'eurostandart', 'state', 'color',
            'coolingType', 'term'
          ];
          const _switch  = [
            'bargain'
          ];
          for (const index in transportType) {
            if ( transportType.hasOwnProperty(index)) {
              const attr = transportType[index];
              if (attr === true) {
                if (off.includes(index) !== true) {
                  let inputType = 'checkbox';
                  let value = 'false';
                  if (dropdowns.includes(index) === true) {
                    inputType = 'dropdown';
                    if (!transportType[index].values) {
                      value = '';
                    } else {
                      value = transportType[index].values;
                    }
                  } else if (_switch.includes(index) === true) {
                    inputType = 'switch';
                  }
                  group[index] = value;
                  this.separated.push(
                    this.formBuilder.group(group)
                  );
                  const sep = {
                    name: index,
                    type: inputType,
                    classes: 'col s12 m4'
                  };
                  this.separatedNames.push(sep);
                  group = {};
                }
              }
            }
          }
        }
      }
    });
    this.SelectInit();
  }

  onChangeType2(_type: string) {
    this.searchService.transportTypes.forEach( transportType => {
      if ( transportType.name === _type ) {
        this.currentBrands2 = transportType.brands;
      }
    });
    this.SelectInit();
  }

  onChangeBrand(brand: string) {
    this.currentBrands.forEach( Brand => {
      if ( Brand.name === brand ) {
        this.currentModels = Brand.models;
      }
    });
    this.SelectInit();
  }

  onChangeBrand2(brand: string) {

    this.currentBrands2.forEach( Brand => {
      if ( Brand.name === brand ) {
        this.currentModels2 = Brand.models;
      }
    });
    this.SelectInit();
  }

  onChangeModel(model: string) {
    this.SelectInit();
  }

  onChangeModel2(model: string) {
    this.SelectInit();
  }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.searchForm.invalid) {
      return;
    }
    const copy = this.func(this.searchForm.value, {});
/*     this.adsService.sendFormDataSearch(copy).subscribe((res: HttpResponse<any>) => {
      if (res.body.length > 0 ) {
        console.log(res.body);
        this.resulted = true;
        this.resultData = res.body;
        const element = document.querySelector('#result');
        const elem = document.querySelector('#colaps-id.collapsible');
        Collapsible.getInstance(elem).close(0);
        element.scrollIntoView({ behavior: 'smooth', block: 'start' });
      } else {
        this.resulted = false;
        this.resultData = [];
      }
    },
    ( err: HttpErrorResponse ) => {
      console.log(err.message);
    }); */
    this.getData().subscribe((res: Ads) => {

      const element = document.querySelector('#result');
      const elem = document.querySelector('#colaps-id.collapsible');
      Collapsible.getInstance(elem).close(0);
      element.scrollIntoView({ behavior: 'smooth', block: 'start' });
      this.resultData = res;
      this.resulted = true;
      console.log(res[0].cubage)
    });
  }

  getData(): Observable<Ads> {
    return this.http.get<Ads>('../../assets/search.json')
    .pipe(
      map(
        (response: Ads) => response as Ads)
      );
  }

  onSubmitAverage() {
    this.Average = Math.floor(Math.random() * 1001);

    this.averageSubmitted = true;
  }

  navigate(id: number) {
    this.router.navigate(['/ad/' + id]);
  }
}
