import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { Ad, Inputs } from '../../shared/models/ads.model';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit, OnDestroy {
  id: number;
  private sub: any;
  private ad: Ad;
  private downloaded: boolean;
  public checkboxes = [
  ];
  constructor(private route: ActivatedRoute, private http: HttpClient, private router: Router) { }
  ngOnInit() {

    this.sub = this.route.params.subscribe(params => {
      this.id = +params['id']; // (+) converts string 'id' to a number
      // In a real app: dispatch action to load the details here.
      if (isNaN(this.id)) {
        this.router.navigate(['/404']);
      }

     this.getData().subscribe((res: Ad) => {
      this.ad = res;
      const input = new Inputs();
      Object.entries(this.ad).forEach(
        ([key, value]) => {
          if (input.checkbox.includes(key) && value !== null) {
            if (value === true) {
              value = 'checked';
            } else if (value === false) {
              value = '';
            }
            this.checkboxes.push({
              'name': key,
              'value': value
            });
          }
        }
      );

      this.downloaded = true;
     });
   });
  }

  get title() {
    if (this.downloaded) {
      const title = this.ad.brand.name + ' ' + this.ad.model.name;
      return title;
    } else {
      return '';
    }
  }

  get price() {
    if (this.downloaded) {
      const price = this.ad.price + ' EUR';
      return price;
    } else {
      return '';
    }
  }
  get year() {
    if (this.downloaded) {
      const year = this.ad.year;
      return year;
    } else {
      return '';
    }
  }
  get body() {
    if (this.downloaded) {
      const body = this.ad.body.name;
      return body;
    } else {
      return '';
    }
  }

  get color() {
    if (this.downloaded) {
      const color = this.ad.color;
      return color;
    } else {
      return '';
    }
  }

  get engine() {
    if (this.downloaded) {
      const engine = this.ad.engine.name;
      return engine;
    } else {
      return '';
    }
  }

  get cubage() {
    if (this.downloaded) {
      const cubage = this.ad.cubage;
      return cubage;
    } else {
      return '';
    }
  }

  get power() {
    if (this.downloaded) {
      const power = this.ad.power;
      return power;
    } else {
      return '';
    }
  }

  get transmission() {
    if (this.downloaded) {
      const transmission = this.ad.transmission.name;
      return transmission;
    } else {
      return '';
    }
  }

  get mileage() {
    if (this.downloaded) {
      const mileage = this.ad.mileage + ' km';
      return mileage;
    } else {
      return '';
    }
  }

  get registration() {
    if (this.downloaded) {
      const registration = this.ad.registration;
      return registration;
    } else {
      return '';
    }
  }

  get additionalInfo() {
    if (this.downloaded) {
      const additionalInfo = this.ad.additionalInfo;
      return additionalInfo;
    } else {
      return '';
    }
  }

  getData(): Observable<Ad> {
    return this.http.get<Ad>('../assets/one.json')
    .pipe(
      map(
        (response: Ad) => response as Ad)
      );

  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}
