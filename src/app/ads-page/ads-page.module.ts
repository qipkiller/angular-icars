import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IndexComponent } from './index/index.component';
import { AdsPageRoutingModule } from './ads-page-routing-module';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { createTranslateLoader } from '../app.module';

// import { ZoomModule } from 'angular-zoom';


@NgModule({
  imports: [
    CommonModule,
    // ZoomModule,
    AdsPageRoutingModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient],
      }
    })
  ],
  declarations: [IndexComponent]
})
export class AdsPageModule { }
