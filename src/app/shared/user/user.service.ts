import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpHeaders, HttpParams
} from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { AssetsService } from '../assets.service';



@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor(private http: HttpClient, private assetsService: AssetsService) {}
  readonly rootUrl = this.assetsService.RootUrl;

  private auth64 = btoa('icars_client:`!RKb]7%CD^!n-@W');
  private tokenHeaders = new HttpHeaders({
    'Content-Type': 'application/x-www-form-urlencoded',
    'Authorization': 'Basic ' + this.auth64
  });

  userAuthentication(userName: string, password: string) {
    const body = new HttpParams()
      .set('username', userName)
      .set('password', password)
      .set('grant_type', 'password');


    return this.http.post(this.rootUrl + '/oauth/token', body,
    {
      headers: this.tokenHeaders,
      withCredentials: true,
      observe: 'response' as 'response'
    })
    .pipe(
      map( data => data ),
      catchError(this.assetsService.handleError)
    );
  }

  logout() {
    return this.http.get(this.rootUrl + '/logout',
      {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
        }),
        observe: 'response' as 'response',
        responseType: 'text'
      }
    )
    .pipe(
      map( data => data),
      catchError(this.assetsService.handleError)
    );
  }

  whoIsAuth() {
    return this.http.get(this.rootUrl + '/authenticated',
      {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
        }),
        observe: 'response' as 'response',
      }
    )
    .pipe(
      map( data => data),
      catchError(this.assetsService.handleError)
    );
  }

  registrationPhyz(values: { [x: string]: string; }) {
    let httpParams = new HttpParams();
    Object.keys(values).forEach(function (key) {
      if (values[key] !== '') {
        httpParams = httpParams.set(key, values[key]);
      }
    });
    return this.http.post(this.rootUrl + '/user/phys/create', httpParams, {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
      }),
      withCredentials: true,
      observe: 'response' as 'response'
    })
    .pipe(
      map( data => data ),
      // catchError(this.assetsService.handleError)
    );
  }


  registrationJur(values: { [x: string]: string; }) {
    let httpParams = new HttpParams();
    Object.keys(values).forEach(function (key) {
      if (values[key] !== '') {
        httpParams = httpParams.set(key, values[key]);
      }
    });
    return this.http.post(this.rootUrl + '/user/jur/create', httpParams, {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
      }),
      withCredentials: true,
      observe: 'response' as 'response'
    })
    .pipe(
      map( data => data ),
      // catchError(this.assetsService.handleError)
    );
  }
}
