import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpHeaders, HttpParams, HttpErrorResponse
} from '@angular/common/http';
import { map, catchError, retry } from 'rxjs/operators';
import { throwError, Observable } from 'rxjs';
import { AssetsService } from '../assets.service';

@Injectable({
  providedIn: 'root'
})
export class AdsService {
  constructor(private http: HttpClient, private assetsService: AssetsService) { }
  readonly rootUrl = this.assetsService.RootUrl;
  private auth64 = btoa('icars_client:`!RKb]7%CD^!n-@W');
  private tokenHeaders = new Headers({
    'Content-Type': 'application/x-www-form-urlencoded',
    'Authorization': 'Basic ' + this.auth64
    });

  getAllTransportType() {
    const token = btoa('icars_client:`!RKb]7%CD^!n-@W');
    console.log(token);
    return this.http.get(this.rootUrl + '/ad/detail/all',
      {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': 'Basic ' + token
        }),
        observe: 'response' as 'response',
        responseType: 'json'
      }
    )
    .pipe(
      map( data => data),
      catchError(this.assetsService.handleError)
    );
  }
  sendFormDataSearch(values) {
    let httpParams = new HttpParams();
    Object.keys(values).forEach(function (key) {
      if (values[key] !== '') {
        httpParams = httpParams.set(key, values[key]);
      }
    });
    return this.http.post(this.rootUrl + '/ad/search', httpParams, {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
      }),
      withCredentials: true,
      observe: 'response' as 'response'
    })
    .pipe(
      map( data => data ),
      catchError(this.assetsService.handleError)
    );
  }


  sendFormDataAddAds(values) {
    let httpParams = new HttpParams();
    Object.keys(values).forEach(function (key) {
      if (values[key] !== '') {
        httpParams = httpParams.set(key, values[key]);
      }
    });
    return this.http.post(this.rootUrl + '/ad/create', httpParams, {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
      }),
      withCredentials: true,
      observe: 'response' as 'response'
    })
    .pipe(
      map( data => data ),
      catchError(this.assetsService.handleError)
    );
  }


}
