export interface Ad {
    id: number;
    creationDateTime: Date;
    transportType: TransportType;
    brand: Brand;
    model: Model;
    body: Body;
    engine: Model;
    transmission: Model;
    registration: number;
    additionalInfo: string;
    eurostandart: string;
    powerFrom?: any;
    powerTo?: any;
    power: number;
    priceFrom?: any;
    priceTo?: any;
    price: number;
    yearFrom?: any;
    yearTo?: any;
    year: number;
    mileageFrom?: any;
    mileageTo?: any;
    mileage: number;
    color: string;
    municipality: string;
    term: number;
    state: string;
    bargain: string;
    axisFrom?: any;
    axisTo?: any;
    axis?: any;
    placesFrom?: any;
    placesTo?: any;
    places?: any;
    cubageFrom?: number;
    cubageTo?: number;
    cubage?: number;
    carryingFrom?: any;
    carryingTo?: any;
    carrying?: any;
    coolingType: string;
    leatherInterior?: any;
    velourInterior?: any;
    alcantarInterior?: any;
    navigationSystem?: any;
    heatingFrontSeats?: any;
    coolingFrontSeats?: any;
    heatingBackSeats?: any;
    coolingBackSeats?: any;
    adjustmentDriver?: any;
    adjustmentFront?: any;
    climateControl?: any;
    climateControlX2?: any;
    climateControlX3?: any;
    climateControlX4?: any;
    conditioning?: any;
    sunroof?: any;
    panoramicSunroof?: any;
    panoramicRoof?: any;
    multimediaBack?: any;
    glassLiftsFront?: any;
    glassLiftsAll?: any;
    usb?: any;
    aux?: any;
    bluetooth?: any;
    keylessGo?: any;
    electricMirror?: any;
    electricMirrorFolding?: any;
    refrigerator?: any;
    multifunctionalSteeringWheel?: any;
    steeringWheelHeating?: any;
    mirrorHeating?: any;
    autoAromatization?: any;
    cruiseControl?: any;
    activeCruiseControl?: any;
    wiperHeating?: any;
    markingSystem?: any;
    signsSystem?: any;
    backCamera?: any;
    circularCamera?: any;
    parktronicFront?: any;
    parktronicBack?: any;
    autoParking?: any;
    deadZonesControl?: any;
    emergencyBrakeSystem?: any;
    electricSeatsAdjustment?: any;
    massageFront?: any;
    massageBack?: any;
    massageAll?: any;
    individualEquipment?: any;
    gpsTracking?: any;
    tintedWindows?: any;
    airSuspension?: any;
    rainSensor?: any;
    lightSensor?: any;
    tiptronic?: any;
    xenonHeadlights?: any;
    ledHeadlights?: any;
    matrixOptics?: any;
    airbagsFront?: any;
    airbagsBack?: any;
    brakeForceDistribution?: any;
    stabilizationElectric?: any;
    stabilizationAuto?: any;
    tirePressureSensor?: any;
    antiSlipSystem?: any;
    brakeDrying?: any;
    distanceControl?: any;
    markingControl?: any;
    brakeAssistSystem?: any;
    descentAssistanceSystem?: any;
    armoredBody?: any;
    doubleGlazedWindows?: any;
    documentsOrder?: any;
    casco?: any;
    alarm?: any;
    immobilizer?: any;
    tradein?: any;
    trade?: any;
    leasing?: any;
    fullyServiced?: any;
    winterTires?: any;
    summerTires?: any;
    newImported?: any;
    serviceBook?: any;
    tuning?: any;
    stOwner?: any;
    ndOwner?: any;
    rdOwner?: any;
    placeX7?: any;
    placeX8?: any;
    longBase?: any;
    shortBase?: any;
    doorsX2?: any;
    doorsX4?: any;
    noAccidents?: any;
    damaged?: any;
    defaultPaint?: any;
    requiresInvestment?: any;
    guarantee?: any;
    taxi?: any;
    disability?: any;
    hearse?: any;
    educational?: any;
    protectiveCovering?: any;
    gasSystem?: any;
  }

  interface TransportType {
    id: number;
    name: string;
    brands: Brand[];
    body: boolean;
    engine: boolean;
    transmission: boolean;
    registration: boolean;
    additionalInfo?: any;
    eurostandart: boolean;
    power: boolean;
    price: boolean;
    year: boolean;
    mileage: boolean;
    color: boolean;
    municipality: boolean;
    term: boolean;
    state: boolean;
    bargain: boolean;
    axis?: any;
    places?: any;
    cubage?: any;
    carrying?: any;
    coolingType: boolean;
    leatherInterior?: any;
    velourInterior?: any;
    alcantarInterior?: any;
    navigationSystem?: any;
    heatingFrontSeats?: any;
    coolingFrontSeats?: any;
    heatingBackSeats?: any;
    coolingBackSeats?: any;
    adjustmentDriver?: any;
    adjustmentFront?: any;
    climateControl?: any;
    climateControlX2?: any;
    climateControlX3?: any;
    climateControlX4?: any;
    conditioning?: any;
    sunroof?: any;
    panoramicSunroof?: any;
    panoramicRoof?: any;
    multimediaBack?: any;
    glassLiftsFront?: any;
    glassLiftsAll?: any;
    usb?: any;
    aux?: any;
    bluetooth?: any;
    keylessGo?: any;
    electricMirror?: any;
    electricMirrorFolding?: any;
    refrigerator?: any;
    multifunctionalSteeringWheel?: any;
    steeringWheelHeating?: any;
    mirrorHeating?: any;
    autoAromatization?: any;
    cruiseControl?: any;
    activeCruiseControl?: any;
    wiperHeating?: any;
    markingSystem?: any;
    signsSystem?: any;
    backCamera?: any;
    circularCamera?: any;
    parktronicFront?: any;
    parktronicBack?: any;
    autoParking?: any;
    deadZonesControl?: any;
    emergencyBrakeSystem?: any;
    electricSeatsAdjustment?: any;
    massageFront?: any;
    massageBack?: any;
    massageAll?: any;
    individualEquipment?: any;
    gpsTracking?: any;
    tintedWindows?: any;
    airSuspension?: any;
    rainSensor?: any;
    lightSensor?: any;
    tiptronic?: any;
    xenonHeadlights?: any;
    ledHeadlights?: any;
    matrixOptics?: any;
    airbagsFront?: any;
    airbagsBack?: any;
    brakeForceDistribution?: any;
    stabilizationElectric?: any;
    stabilizationAuto?: any;
    tirePressureSensor?: any;
    antiSlipSystem?: any;
    brakeDrying?: any;
    distanceControl?: any;
    markingControl?: any;
    brakeAssistSystem?: any;
    descentAssistanceSystem?: any;
    armoredBody?: any;
    doubleGlazedWindows?: any;
    documentsOrder?: any;
    casco?: any;
    alarm?: any;
    immobilizer?: any;
    tradein?: any;
    trade?: any;
    leasing?: any;
    fullyServiced?: any;
    winterTires?: any;
    summerTires?: any;
    newImported?: any;
    serviceBook?: any;
    tuning?: any;
    stOwner?: any;
    ndOwner?: any;
    rdOwner?: any;
    placeX7?: any;
    placeX8?: any;
    longBase?: any;
    shortBase?: any;
    doorsX2?: any;
    doorsX4?: any;
    noAccidents?: any;
    damaged?: any;
    defaultPaint?: any;
    requiresInvestment?: any;
    guarantee?: any;
    taxi?: any;
    disability?: any;
    hearse?: any;
    educational?: any;
    protectiveCovering?: any;
    gasSystem?: any;
  }

  interface Brand {
    id: number;
    name: string;
    models: Model[];
  }

  interface Model {
    id: number;
    name: string;
  }
  interface Body {
    id: number;
    name: string;
  }

  export class Inputs {
    checkbox = [
      'usb', 'leatherInterior' , 'velourInterior',
      'alcantarInterior', 'navigationSystem', 'heatingFrontSeats',
      'coolingFrontSeats', 'heatingBackSeats', 'coolingBackSeats',
      'adjustmentDriver', 'adjustmentFront', 'climateControl',
      'climateControlX2', 'climateControlX3', 'climateControlX4',
      'conditioning', 'sunroof', 'panoramicSunroof', 'panoramicRoof',
      'multimediaBack', 'glassLiftsFront', 'glassLiftsAll',
      'aux', 'bluetooth', 'keylessGo', 'electricMirror',
      'electricMirrorFolding', 'refrigerator', 'multifunctionalSteeringWheel',
      'steeringWheelHeating', 'mirrorHeating', 'autoAromatization',
      'cruiseControl', 'activeCruiseControl', 'wiperHeating',
      'markingSystem', 'signsSystem', 'backCamera', 'circularCamera',
      'parktronicFront', 'parktronicBack', 'autoParking', 'deadZonesControl',
      'emergencyBrakeSystem', 'electricSeatsAdjustment', 'massageFront',
      'massageBack', 'massageAll', 'individualEquipment', 'gpsTracking',
      'tintedWindows', 'airSuspension', 'rainSensor', 'lightSensor',
      'tiptronic', 'xenonHeadlights', 'ledHeadlights', 'matrixOptics',
      'airbagsFront', 'airbagsBack', 'brakeForceDistribution',
      'stabilizationElectric', 'stabilizationAuto', 'tirePressureSensor',
      'antiSlipSystem', 'brakeDrying', 'distanceControl', 'markingControl',
      'brakeAssistSystem', 'descentAssistanceSystem', 'armoredBody', 'doubleGlazedWindows',
      'documentsOrder', 'casco', 'alarm', 'immobilizer', 'tradein', 'trade', 'leasing',
      'fullyServiced', 'winterTires', 'summerTires', 'newImported',
      'serviceBook', 'tuning', 'stOwner', 'ndOwner', 'rdOwner', 'placeX7',
      'placeX8', 'longBase', 'shortBase', 'doorsX2', 'doorsX4', 'noAccidents',
      'damaged', 'defaultPaint', 'requiresInvestment', 'guarantee', 'taxi',
      'disability', 'hearse', 'educational', 'protectiveCovering', 'gasSystem'
    ];
  }

  export interface Ads {
    [index: number]: Ad;
  }
