import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class LanguageService {
  constructor(private translate: TranslateService) {
    translate.use('ru');
  }

  get currentLang() { return this.translate.currentLang; }

  use(language) {
    return this.translate.use(language).pipe(
      map( lang => lang)
    );
  }
}
