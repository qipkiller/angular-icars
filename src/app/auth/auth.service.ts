import { Injectable } from '@angular/core';
import { UserService } from '../shared/user/user.service';
import { JwtHelperService } from '@auth0/angular-jwt';
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(public jwtHelper: JwtHelperService) { }
  user: boolean;
  userr = {};

  pushUser(User: { [s: string]: {}; } | ArrayLike<{}>) {
    Object.entries(User).forEach(
      ([key, value]) => {
        this.userr[key] = value;
        if (key == 'login' && typeof(value) == 'string') {
          console.log(value)
          sessionStorage.setItem('login', value);
        }
      }
    );
    sessionStorage.setItem('userToken', 'f53dc9325c5f4a23a0c9ae909a278da');
    
    this.user = true;
  }
  removeUser() {
    this.userr = {};
    sessionStorage.removeItem('userToken');
    sessionStorage.removeItem('login');
    this.user = false;
  }
  isLogin(): boolean {
    return this.userr['name'];
  }
  getUser(): object {
    return this.userr;
  }

  public isAuthenticated(): boolean {

    const token = sessionStorage.getItem('userToken');

    // Check whether the token is expired and return
    // true or false
    // console.log(this.jwtHelper.isTokenExpired(token));
    //return !this.jwtHelper.isTokenExpired(token);
    return token ? true : false;
  }
}
